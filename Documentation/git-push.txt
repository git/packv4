git-push(1)
===========

NAME
----
git-push - Update remote refs along with associated objects


SYNOPSIS
--------
[verse]
'git-push' [--all] [--tags] [--receive-pack=<git-receive-pack>]
           [--repo=all] [-f | --force] [-v] [<repository> <refspec>...]

DESCRIPTION
-----------

Updates remote refs using local refs, while sending objects
necessary to complete the given refs.

You can make interesting things happen to a repository
every time you push into it, by setting up 'hooks' there.  See
documentation for gitlink:git-receive-pack[1].


OPTIONS
-------
<repository>::
	The "remote" repository that is destination of a push
	operation.  See the section <<URLS,GIT URLS>> below.

<refspec>::
	The canonical format of a <refspec> parameter is
	`+?<src>:<dst>`; that is, an optional plus `+`, followed
	by the source ref, followed by a colon `:`, followed by
	the destination ref.
+
The <src> side can be an
arbitrary "SHA1 expression" that can be used as an
argument to `git-cat-file -t`.  E.g. `master~4` (push
four parents before the current master head).
+
The local ref that matches <src> is used
to fast forward the remote ref that matches <dst>.  If
the optional plus `+` is used, the remote ref is updated
even if it does not result in a fast forward update.
+
Note: If no explicit refspec is found, (that is neither
on the command line nor in any Push line of the
corresponding remotes file---see below), then all the
refs that exist both on the local side and on the remote
side are updated.
+
`tag <tag>` means the same as `refs/tags/<tag>:refs/tags/<tag>`.
+
A parameter <ref> without a colon pushes the <ref> from the source
repository to the destination repository under the same name.
+
Pushing an empty <src> allows you to delete the <dst> ref from
the remote repository.

\--all::
	Instead of naming each ref to push, specifies that all
	refs be pushed.

\--tags::
	All refs under `$GIT_DIR/refs/tags` are pushed, in
	addition to refspecs explicitly listed on the command
	line.

\--receive-pack=<git-receive-pack>::
	Path to the 'git-receive-pack' program on the remote
	end.  Sometimes useful when pushing to a remote
	repository over ssh, and you do not have the program in
	a directory on the default $PATH.

\--exec=<git-receive-pack>::
	Same as \--receive-pack=<git-receive-pack>.

-f, \--force::
	Usually, the command refuses to update a remote ref that is
	not an ancestor of the local ref used to overwrite it.
	This flag disables the check.  This can cause the
	remote repository to lose commits; use it with care.

\--repo=<repo>::
	When no repository is specified the command defaults to
	"origin"; this overrides it.

\--thin, \--no-thin::
	These options are passed to `git-send-pack`.  Thin
	transfer spends extra cycles to minimize the number of
	objects to be sent and meant to be used on slower connection.

-v::
	Run verbosely.

include::urls-remotes.txt[]


Examples
--------

git push origin master::
	Find a ref that matches `master` in the source repository
	(most likely, it would find `refs/heads/master`), and update
	the same ref (e.g. `refs/heads/master`) in `origin` repository
	with it.

git push origin :experimental::
	Find a ref that matches `experimental` in the `origin` repository
	(e.g. `refs/heads/experimental`), and delete it.

git push origin master:satellite/master::
	Find a ref that matches `master` in the source repository
	(most likely, it would find `refs/heads/master`), and update
	the ref that matches `satellite/master` (most likely, it would
	be `refs/remotes/satellite/master`) in `origin` repository with it.

Author
------
Written by Junio C Hamano <junkio@cox.net>, later rewritten in C
by Linus Torvalds <torvalds@osdl.org>

Documentation
--------------
Documentation by Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
