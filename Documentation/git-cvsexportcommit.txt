git-cvsexportcommit(1)
======================

NAME
----
git-cvsexportcommit - Export a single commit to a CVS checkout


SYNOPSIS
--------
'git-cvsexportcommit' [-h] [-u] [-v] [-c] [-P] [-p] [-a] [-d cvsroot] [-f] [-m msgprefix] [PARENTCOMMIT] COMMITID


DESCRIPTION
-----------
Exports a commit from GIT to a CVS checkout, making it easier
to merge patches from a git repository into a CVS repository.

Execute it from the root of the CVS working copy. GIT_DIR must be defined.
See examples below.

It does its best to do the safe thing, it will check that the files are
unchanged and up to date in the CVS checkout, and it will not autocommit
by default.

Supports file additions, removals, and commits that affect binary files.

If the commit is a merge commit, you must tell git-cvsexportcommit what parent
should the changeset be done against.

OPTIONS
-------

-c::
	Commit automatically if the patch applied cleanly. It will not
	commit if any hunks fail to apply or there were other problems.

-p::
	Be pedantic (paranoid) when applying patches. Invokes patch with
	--fuzz=0

-a::
	Add authorship information. Adds Author line, and Committer (if
	different from Author) to the message.

-d::
	Set an alternative CVSROOT to use.  This corresponds to the CVS
	-d parameter.  Usually users will not want to set this, except
	if using CVS in an asymmetric fashion.

-f::
	Force the merge even if the files are not up to date.

-P::
	Force the parent commit, even if it is not a direct parent.

-m::
	Prepend the commit message with the provided prefix.
	Useful for patch series and the like.

-u::
	Update affected files from CVS repository before attempting export.

-v::
	Verbose.

EXAMPLES
--------

Merge one patch into CVS::
+
------------
$ export GIT_DIR=~/project/.git
$ cd ~/project_cvs_checkout
$ git-cvsexportcommit -v <commit-sha1>
$ cvs commit -F .mgs <files>
------------

Merge pending patches into CVS automatically -- only if you really know what you are doing::
+
------------
$ export GIT_DIR=~/project/.git
$ cd ~/project_cvs_checkout
$ git-cherry cvshead myhead | sed -n 's/^+ //p' | xargs -l1 git-cvsexportcommit -c -p -v
------------

Author
------
Written by Martin Langhoff <martin@catalyst.net.nz>

Documentation
--------------
Documentation by Martin Langhoff <martin@catalyst.net.nz>

GIT
---
Part of the gitlink:git[7] suite
