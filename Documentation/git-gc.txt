git-gc(1)
=========

NAME
----
git-gc - Cleanup unnecessary files and optimize the local repository


SYNOPSIS
--------
'git-gc' [--prune] [--aggressive]

DESCRIPTION
-----------
Runs a number of housekeeping tasks within the current repository,
such as compressing file revisions (to reduce disk space and increase
performance) and removing unreachable objects which may have been
created from prior invocations of gitlink:git-add[1].

Users are encouraged to run this task on a regular basis within
each repository to maintain good disk space utilization and good
operating performance.

OPTIONS
-------

--prune::
	Usually `git-gc` packs refs, expires old reflog entries,
	packs loose objects,
	and removes old 'rerere' records.  Removal
	of unreferenced loose objects is an unsafe operation
	while other git operations are in progress, so it is not
	done by default.  Pass this option if you want it, and only
	when you know nobody else is creating new objects in the
	repository at the same time (e.g. never use this option
	in a cron script).

--aggressive::
	Usually 'git-gc' runs very quickly while providing good disk
	space utilization and performance.  This option will cause
	git-gc to more aggressively optimize the repository at the expense
	of taking much more time.  The effects of this optimization are
	persistent, so this option only needs to be used occasionally; every
	few hundred changesets or so.

Configuration
-------------

The optional configuration variable 'gc.reflogExpire' can be
set to indicate how long historical entries within each branch's
reflog should remain available in this repository.  The setting is
expressed as a length of time, for example '90 days' or '3 months'.
It defaults to '90 days'.

The optional configuration variable 'gc.reflogExpireUnreachable'
can be set to indicate how long historical reflog entries which
are not part of the current branch should remain available in
this repository.  These types of entries are generally created as
a result of using `git commit \--amend` or `git rebase` and are the
commits prior to the amend or rebase occurring.  Since these changes
are not part of the current project most users will want to expire
them sooner.  This option defaults to '30 days'.

The optional configuration variable 'gc.rerereresolved' indicates
how long records of conflicted merge you resolved earlier are
kept.  This defaults to 60 days.

The optional configuration variable 'gc.rerereunresolved' indicates
how long records of conflicted merge you have not resolved are
kept.  This defaults to 15 days.

The optional configuration variable 'gc.packrefs' determines if
`git gc` runs `git-pack-refs`.  Without the configuration, `git-pack-refs`
is not run in bare repositories by default, to allow older dumb-transport
clients fetch from the repository,  but this will change in the future.

The optional configuration variable 'gc.aggressiveWindow' controls how
much time is spent optimizing the delta compression of the objects in
the repository when the --aggressive option is specified.  The larger
the value, the more time is spent optimizing the delta compression.  See
the documentation for the --window' option in gitlink:git-repack[1] for
more details.  This defaults to 10.

See Also
--------
gitlink:git-prune[1]
gitlink:git-reflog[1]
gitlink:git-repack[1]
gitlink:git-rerere[1]

Author
------
Written by Shawn O. Pearce <spearce@spearce.org>

GIT
---
Part of the gitlink:git[7] suite
